# Script Backup

Notre Script récupére le fichier de conf ".backup.conf"
il insére les variable de ce fichier 

le voici : 

    bash #!/bin/bash
### Avec la commande source je vais chercher mon fichier de conf. avec ses variables.
    source .backupconf
### On commence par lancer la sauvegarde , elle renvoit le backup vers la destination, avec la nomenclature créé.

    >if [ $ALL_DATABASES == "yes" ]; then
    $MYSQLDUMP -u$USERSQL -p$MDPSQL --all-databases > $FILENAMEBDD
    elif [$ALL_DATABASES == "no"]; then
   $MYSQLDUMP -u$USERSQL -p$MDPSQL --databases  $NAMEDATABASE > $FILENAMEBDD
    fi

### fichier log qui recoit les informations, 

    echo  le backup est bien effectué $(date +%Y_%m_%d_%T) >> $LOGFILE

* Pour résumer notre script sauvegarde toutes les Database ou une seul avec une nomenclature classé par date "backupannée_mois_jour_heure.sql dans un répertoire nommé backup dans le $HOME
* un Log de chaque action va se rajouter au répertoire /varlog/backup/backup.log par incrémentation.

je n'ai pas réussi a configurer le retour d'erreur :
> message d'erreur sur ligne "commande introuvable" j'utilisais une boucle "if avec la variable $?"

Souci également pour configurer le stockage d'un nombre limité se backups.

# Conclusion :
C'était la premiere fois que je m'essayais au script sur Bash, c'est quelque chose d'assez intéressant que je compte approfondir.