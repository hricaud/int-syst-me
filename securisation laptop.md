# Sécurisation du laptop

Laptop Asus 17 pouces.
- **i7-8750H Cpu @ 2,20Ghz**
-  **16 go de ram**
- **Disque C "system" et Disque D "Data"**

**Configuration**

Le Poste est configuré sur Windows 10, les données sensibles sont classé sur le disque D "Data",
le disque C contient l'OS et certaine données.
Les données sont sauvegardés grâce a Iperius Backup qui sauvegarde sur un disque dur externe tous les dimanches.

**2 sauvegardes** sont configurées : 
- sauvegarde image systéme
> sauvegarde tous les dimanches soirs.
- sauvegarde données
> sauvegardes tous les mercredi et dimanche soirs.

Pour la gestion des mots de passe le Pc est configuré avec **Enpass**.
Il permet de gérer : 
- La générations de Mot de passe
- d'avoir un fichier de conf. cryptée pour migrer les Mots de passe d'un poste à un autre.
- de configurer via un emplacement cloud ou un serveur webdav.

=================================================================================