# Script Bash 

Nous mettons en place un script Bash qui permet le Backup d'un BDD SQL avec la commande Mysqldump.

C'était un gros défi pour ma part pour n'avoir jamais fait de script.

Il est mis en place avec un fichier de conf. qui contient les variables :

####  Script bash pour  backup de BDD.  ####
#### on va mettre un dossier pour save les backups.
>BACKUP_DIR="$HOME/backup/"
##### Identifiant SQL (les ID des users à modifier)
>USERSQL="root"
MDPSQL="root"
#### COMMANDE SQL (raccourci des commandes mysql et mysqldump)
>MYSQL=/usr/bin/mysql
MYSQLDUMP=/usr/bin/mysqldump
#### Les fichiers antérieur à Garder
>BACKUP_DAY="4"
#### Savoir si on sauvegarde toutes les Database 
>ALL_DATABASES="yes"
#### Ou avec le nom de la Database
>NAMEDATABASE="Base_Backup"
#### On créé le nom du fichier avec la date
>DATE="backup$(date +%Y_%m_%d_%T).sql"
#### On regroupe la localisation du backup  + le nom du fichier sauvegardé.
>FILENAMEBDD="$BACKUP_DIR $DATE"
#### Stockage des logs
>LOGFILE="/var/log/backup/backup.log"