# Vmware ESXi

## Projet fil rouge - Esxi 

### J'ai choisi de présenter Vmware Esxi 

> L'objectif et de monter 2 serveurs en Higth Availability et Fault Tolerance et d'apprendre à maitriser Vsphere
Etapes : 

- Monter les 2 Vms ESXi
- Monter un SAN ou NAS pour stocker le datastore (vm du cluster)
- Monter un Cluster HA(Higth Availability)
- Utiliser des protocoles de stockages tels que ISiCSi
- Créer un réseau virtuel adéquate
- Et assurer un Fault Tolerance 

Pour commencer je présente rapidement Vmware Esxi

* Présentation 

Esxi est un hyperviseur de type 1 ‘Bare  metal’ dit natif. Il est développé par VMWARE	 
Il permet de gérer, d’installer et virtualiser des ordinateurs et des serveurs.
Il s’installe directement sur une couche matérielle.
Il permet de concentrer au maximum les ressources pour les machines virtuelles.

* Le réseau 
Il se fractionne en différentes parties avec des termes spécifiques :
>Vmnic : carte réseaux physique du serveur Esxi
>
>Vmkernel : Port qui à besoin d’une adresse IP et au moins d’une carte réseau physique connectée.
Il est utile à différentes fonctions.

> Vm Network (virtual port) : port des Vm.

> vSwitch : switch virtuel configurable.

> Vnic : carte réseau des Vms.

_______________________________________________________________________________________________________________________________________________________________________________

### Installation des Vm

- souci rencontré sur virtualbox ( pendant l'installation des Esx il cherche une carte physique) , impossible de placer une configuration adéquate
- je décide de passer sur Vmware workstation pro = Impeccable (aucun soucis)

J'installe mes machine sur une carte hostonly en Ip 192.168.218.0 /24
Une autre interface réseau a été rajouté en 192.168.88.0/24 elle servira pour la connexion Iscsi entre l'environnement de stockage et le serveur Esxi.
L'installation d'un ESX est plutot rapide : 
> Config 2 cpu & 2 cores

> 4 go de ram

> 40 Go de stockage

On créé un compte Root pour la connexion sur le serveur ( on peut créer plusieurs comptes via l'interface GUI) 

* Le 1er serveur Esxi est sur l'adresse 192.168.218.155
* le 2eme serveur Esxi est sur l'adresse 192.1168.218.165
* Téléchargement de Vsphere.

Test de connexion sur les 2 serveurs > tout est Ok.

* Soucis Présent : 

Par manque de licence, je ne peux créer de cluster et faire de la HA.
En attente d'une solution de contournement je pars sur une configuration avec un seul Vsphere.

Je désinstalle le 2eme Esxi en attendant, et je vais explorer d'autre solution d'intégration.

_______________________________________________________________________________________________________________________________________________________________________________

### Schéma Réseau

Voici la configuration à mettre en place 

![](./images/schema_projet.png)
_______________________________________________________________________________________________________________________________________________________________________________

### Les Accés

Pour accéder aux Gui des serveurs 

Serveur Vsphere 192.168.218.155 
Le San : 192.168.218.110
Le Nas Backup : 192.168.218.111

Les serveurs Dns sont accessiblent en SSH : 
* le master : 192.168.218.88 > avec le compte Admin
* le slave  : 192.168.218.89 > avec le compte Admin également


_______________________________________________________________________________________________________________________________________________________________________________


### Freenas 
Freenas configuré avec 5 disques :

> un pour le systéme 20go 
> 4 de 40Go pour le stockage Raid 5e (pour la sécurité)


2 carte réseaux comme les serveurs Esxi 
> l'interface web est sur l'adresse 192.168.218.110

> l'interface de connexion Iscsi entre le serveur esx et freenas est configuré sur la plage 192.168.88.0/24.

Une fois connecté sur l'interface freenas web, je configure l'interface ip 192.168.88.110 qui sera le lien pour le portail Iscsi.
> En option je mes le MTU à 9000 pour les jumbo frames

On créé un Pool (raidz) à 4 disques qui revient a un raid5 avec un disque prét a remplacer le 1er qui tombe 


J'installe un deuxiéme freenas pour le Backup

> configurer en raid 1 

> Adressage Ip : 192.168.218.111

Je vais préparer une syncro Rsync entre les 2 freenas pour récupérer ma banque de données.

_______________________________________________________________________________________________________________________________________________________________________________

### Configuration de la Target Iscsi

Pour configurer un lien Iscsi nous avons besoin d'un initiateur et d'un target, dans notre cas le Freenas(qui à un rôle de San) est le target et notre hôte Esxi est l'initiateur.
On commence par configurer le target :
> nom : iqn.2005-10.org.freenas.ctl

> On configure le Portal > 192.168.88.110 port 3260

> on configure les entrés au San (on accepte toutes les entrées pour la configuration) / a la fin quand tous sera fonctionnera on mettra seulement la node de l'hote esx.

> on donne un nom à la target on garde "iqn.2005-10.org.freenas.ctl", avec le Port Id 1

> On va choisir l'extent (le disque qui sera gérer par le protocol de stockage). On prend notre pool créé et on ajoute un nom a l'extent "Extent_Iscsi".

> Et pour finir on associe la target à l'extent et on affecte un numero à notre LUN.

_______________________________________________________________________________________________________________________________________________________________________________


### Intiateur Isci

Avant de configurer notre initiateur nous créons un **vswitch** sur sur notre hôte esx, nous lui affectons un port Vmkernel que nous nommons **VmKernel_Iscsi**.
Il faut que notre lien Iscsi soit seul sur la bande passante la bonne pratique aurait était de mettre 2 liens physique en fibre channel en accés a notre SAN.
Vu que nous sommes sur des vms juste un lien sera suffisant. Nous créons tout de meme un **vswitch** pour etre en lien avec notre deuxiéme interface physique 192.168.88.0/24.

Nous adressons le VmKernel : 192.168.88.155/24

Notre lien Iscsi sera isoler et sera consacré qu'au stockage.

Pour l'initiateur, la configuration se fait sur l'hôte esxi.
Dans configuration : 

* Paramétre de stockage
* on ajoute un nouvel adapteur de stockage Iscsi
* On lui rajoute l'ip 162.168.88.110:3260
* On voit le nom de notre target qui apparait.

_______________________________________________________________________________________________________________________________________________________________________________

### Création D'une Datastore 

Nous devons créé une datastore (banque de donnée) pour stocker nos Machines virtuelles, c'est pour cela que nous avons configuré une route Iscsi avec un San. 
Le but étant d'avoir nos Vm sur un pool de disque qui assure une disponibilité, sécurité et continuité de service en cas de souci sur notre hote esx.
Nous allons dans les paramétres de stockages : 

* Ajout d'un nouveau stockage 
* nous avons le Lun vers notre freenas (SAN) ou les disques présent sur notre server esx
* on choisit le Lun créé sur le freenas (SAN)

On nomme notre datastore **datastore_Lun1**

Nous créons une deuxieme datastore avec le disque dur présent de notre esx pour stocker les Iso d'installations des futurs vm, on pourrait également inserer un autre disque dur. 
Le but de cette manoeuvre et de créer un répertoire d'accés au iso d'installation des Vm et de mettre des droit d'accés au users qui pourrait installer des Vm. 
Ce qui permet un meilleur contrôle de notre serveur Esx.

On nomme le datastore **Datastore_Local**

![](./images/datastore.png)
_______________________________________________________________________________________________________________________________________________________________________________

### DNS 

Mise en place d'un Serveur Dns sur centos

Le master sur 192.168.218.88
Le slave sur 192.168.218.89

Nous mettons en place comme nom : 

Admin.esx.domain.com 

San.domain.com

Backup.domain.com

> coté configuration on installe bind 

``` yum install bind ```  
 
```yum install bind-utils ```

> On configure le fichier de conf named.conf dans /etc/

```
zone "domain.com" IN {
        type master;
        file "domain.com.db";
      allow-update { none; };
zone "218.168.192.in-addr.arpa" IN {
                type master;
                file "218.168.192.db";
                allow-update { none; };
```

On configure le deuxieme DNS : le slave 

Le slave est récursif il est configuré sur l'adresse 192.168.218.89.

_________________________________________________________________________________________________________________________________________________________________________________



